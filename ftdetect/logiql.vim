" filetypes for LogiQL files ending with .logic and .lb

au BufRead,BufNewFile *.logic   setfiletype logiql

au BufRead,BufNewFile *.lb      setfiletype logiql_script
