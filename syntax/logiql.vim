if exists("b:current_syntax")
	finish
endif

syn clear

syn region lbComment start="/\*" end="\*/"
syn match lbComment "//.*"
hi link lbComment Comment

let b:current_syntax = "logiql" 
